import 'dart:async';
import 'dart:developer';

import 'package:flutter_test/flutter_test.dart';
import 'package:easyping/easyping.dart';

void main() {
  test('Transform Unix ', () async{
    StreamController<String> s = StreamController<String>();
    var trs = s.stream.transform(unixTransformer);
    trs.listen((event) {
      expect(event.time, Duration(milliseconds: 17));
    });
    s.sink.add('64 bytes from fra16s14-in-x0e.1e100.net (2a00:1450:4001:81a::200e): icmp_seq=2 ttl=57 time=17.1 ms');
    s.close();
  });

  test('Transform windows ', () async{
    StreamController<String> s = StreamController<String>();
    var trs = s.stream.transform(windowsTransformer);
    trs.listen((event) {
      expect(event.time, Duration(milliseconds: 36));
    });
    s.sink.add('Reply from 216.58.208.46: Bytes=32 time=36ms TTL=53');
    s.close();
  });
  
  test('pinging google', () async {
    var googlePing = await ping('google.com');
    expect(googlePing, greaterThan(0));
  });
}
