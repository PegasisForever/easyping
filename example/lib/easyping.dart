library easyping;

import 'dart:async';
import 'dart:io';
import 'dart:convert';

/// Information about ping response
class PingInfo {
  Duration time;

  /// Creates a new PingInfo with the given response time
  PingInfo._new(this.time);
}

var _regexUnix = RegExp(r"time=(\d+).");
var _regexWindows = RegExp(r"time=(\d+)ms");

/// Converts the response on Unix systems into PingInfos using regular expressions
StreamTransformer<String, PingInfo> _unixTransformer =
StreamTransformer.fromHandlers(handleData: (data, sink) {
  if (data.contains("bytes from")) {
    var match = _regexUnix.firstMatch(data);
    sink.add(PingInfo._new(Duration(milliseconds: int.parse(match.group(1)))));
  }
});

/// Converts the response on Windows systems into PingInfos using regular expressions
StreamTransformer<String, PingInfo> _windowsTransformer =
StreamTransformer.fromHandlers(handleData: (data, sink) {
  if (data.startsWith("Reply from")) {
    var match = _regexWindows.firstMatch(data);
    sink.add(PingInfo._new(Duration(milliseconds: int.parse(match.group(1)))));
  }
});

/// Pings host [address]. Returns median value as [double] in Milliseconds
Future<double> ping(String address, {int times = 4}) async {
  var process = await Process.start("ping", ['-c $times', '-i 1', address]);
  var baseStream =
  process.stdout.transform(utf8.decoder).transform(LineSplitter());

  double totalTime = 0;
  if (Platform.isWindows) {
    await baseStream.transform<PingInfo>(_windowsTransformer).forEach((ping) {
      totalTime += ping.time.inMilliseconds;
    });
  } else {
    await baseStream.transform<PingInfo>(_unixTransformer).forEach((ping) {
      totalTime += ping.time.inMilliseconds;
    });
  }
  return totalTime / times;
}
